<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Sidebar'),
  'category' => t('Custom Layouts'),
  'icon' => 'two-sidebar.png',
  'theme' => 'two-sidebar',
  'css' => 'two-sidebar.css',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'banner' => t('Banner'),
    'breadcrumb' => t('Breadcrumb'),
    'content-top' => t('Content Top'),
    'content-left' => t('Content Left'),
    'content-right' => t('Content Right'),
    'content-bottom' => t('Content Bottom'),
    'content-footer-left' => t('Content Footer Left'),
    'content-footer-right' => t('Content Footer Right'),
  ),
);

?>

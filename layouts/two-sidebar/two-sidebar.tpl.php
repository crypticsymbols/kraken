<?php
// This is VERY important!

// Well, not really.

// I created this template originally as
// part of a different project.  It had
// to grow and evolve over time, and
// gradually became a monstrosity.  It
// happens to the best of us.

// Moral is:  Please try to write better 
// code than this.  It's just here as an
// example.

// Thanks!

// Aaron Smith, http://aaronsmith.co/
?>

<?php $classes = ''; $page_classes = ''; ?>

<?php if (!$content['content-right']): ?>
    <?php $classes .= 'no-rightbar'; ?>
<?php endif; ?>

<?php if (!$content['sidebar']): ?>
    <?php $page_classes .= 'fullright'; ?>
<?php endif; ?>

<?php if ($content['banner']): ?>
    <div class="bannerwrap">
        <div id="banner">
            <?php print $content['banner']; ?>
        </div>
    </div>
<?php endif; ?>
<?php if ($content['breadcrumb']): ?>
    <div class="breadcrumbwrap">
        <div id="breadcrumb">
            <?php print $content['breadcrumb']; ?>
        </div>
    </div>
<?php endif; ?>

<div id="page-content">
    <div id="main-content" class="<?php print $page_classes; ?>">
        <?php if ($content['content-top']): ?>
            <div class="row">
                <div class="full"><?php print $content['content-top']; ?></div>
            </div>
        <?php endif; ?>
        <div class="row <?php print $classes; ?>">
            <div class="col sixty"><?php print $content['content-left']; ?></div>
            <?php if ($content['content-right']): ?>
                <div class="col forty"><?php print $content['content-right']; ?></div>
            <?php endif; ?>
        </div>
        <?php if ($content['content-bottom']): ?>
            <div class="row">
                <div class="full"><?php print $content['content-bottom']; ?></div>
            </div>
        <?php endif; ?>
        <?php if ($content['content-footer-left'] || $content['content-footer-right']): ?>
            <div class="row">
                <div class="col fifty"><?php print $content['content-footer-left']; ?></div>
                <div class="col fifty"><?php print $content['content-footer-right']; ?></div>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($content['sidebar']): ?>
        <div id="sidebar">
            <div><?php print $content['sidebar']; ?></div>
        </div>
    <?php endif; ?>
</div>
<div class="clearfix"></div>

#!/bin/bash
# Script to generate a panels pane
# from the SAMPLEPANE starter.
#

read -p "Enter the same of the custom layout: " layoutname
echo "Creating pane $layoutname..."

mkdir $layoutname ;

cat SAMPLELAYOUT/SAMPLELAYOUT.inc | \
sed -e 's/SAMPLELAYOUT/'$layoutname'/' > $layoutname/$layoutname.inc

cat SAMPLELAYOUT/SAMPLELAYOUT.tpl.php | \
sed -e 's/SAMPLELAYOUT/'$layoutname'/' > $layoutname/$layoutname.tpl.php

cat SAMPLELAYOUT/SAMPLELAYOUT.png | \
sed -e 's/SAMPLELAYOUT/'$layoutname'/' > $layoutname/$layoutname.png

cat SAMPLELAYOUT/SAMPLELAYOUT.css | \
sed -e 's/SAMPLELAYOUT/'$layoutname'/' > $layoutname/$layoutname.css

echo "Finished creating $panelname custom layout."
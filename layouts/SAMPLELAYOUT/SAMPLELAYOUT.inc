<?php

// Plugin definition
$plugin = array(
  'title' => t('SAMPLELAYOUT'),
  'category' => t('Custom Layouts'),
  'icon' => 'SAMPLELAYOUT.png',
  'theme' => 'SAMPLELAYOUT',
  'css' => 'SAMPLELAYOUT.css',
  'regions' => array(
    'main' => t('Main Region'),
  ),
);

?>

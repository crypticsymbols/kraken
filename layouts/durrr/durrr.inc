<?php

// Plugin definition
$plugin = array(
  'title' => t('durrr'),
  'category' => t('Custom Layouts'),
  'icon' => 'durrr.png',
  'theme' => 'durrr',
  'css' => 'durrr.css',
  'regions' => array(
    'main' => t('Main Region'),
  ),
);

?>

<?php
// As flexible as we want to stay, the reality is that headers and footers
// are a fact of life for most websites, and they are usually the same site-wide.
// The block system really just makes more sense for that because adding them
// to every panel variant will get pretty tiresome.
// 
// The "top" region is here because I use it a lot for my own projects, and
// if usually contains a search form, utility navigation, or some branding
// elements.
?>

<header>
	<?php if ($page['top']): ?>
		<div id="top">
			<?php print render($page['top']) ?>
		</div>
	<?php endif; ?>
	<nav id="main-nav">
		<?php print render($page['nav']) ?>
	</nav>
</header>

<?php
// Messages and tabs are pretty much the same.  We want to make sure they can show
// up anywhere and without thinking about them on a variant-by-variant basis.
?>

<?php if ($messages): ?>
    <?php print $messages; ?>
<?php endif; ?>
<?php if ($tabs): ?>
  <div class="tabs"><?php print render($tabs); ?></div>
<?php endif; ?>

<?php
// Everything that isn't in the header or the footer is rendered through panels.
?>
<?php print render($page['content']) ?>

<?php if ($page['footer']): ?>
	<footer>
		<div id="footer">
			<?php print render($page['footer']) ?>
		</div>
	</footer>
<?php endif; ?>

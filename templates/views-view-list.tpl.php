<?php 

// Borrowed from the Mothership theme http://drupal.org/project/mothership

?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix; ?>
    <?php foreach ($rows as $id => $row): ?>
    <?php //$zebra = ($zebra % 2) ? 'even' : 'odd'; ?>
      <li>
        <?php print $row; ?>
      </li>
    <?php endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
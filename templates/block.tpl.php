
<div class="<?php print $classes .  $attributes; ?>">

  <?php print render($title_prefix); ?>
  <?php if ($block->subject): ?>
    <h2><?php print $block->subject; ?></h2>
  <?php endif;?>
  <?php print render($title_suffix); ?>

  <?php print $content ?>
<div>
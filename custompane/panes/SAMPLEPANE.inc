<?php

$plugin = array(
  // the title in the admin
  'title' => t('SAMPLEPANE Custom Content Pane'),
  // no one knows if "single" defaults to FALSE...
  'single' => TRUE,
  // Say where we want our pane to show in the Panels UI.
  'category' => array(t('Custom Panes'), -9),
  // We can add required contexts if we want to - node, user, term, entity, etc.
  //'required context' => new ctools_context_required(t('Node'), 'node'),
  // Which function is responsible for rendering this pane?
  'render callback' => 'custompane_SAMPLEPANE_content_type_render'
);


/**
 * Run-time rendering of the body of the block (content type)
 */
function custompane_SAMPLEPANE_content_type_render($subtype, $conf, $args, $context) {

  // You can add js and css files here.  They get added to the page as it's getting rendered.
  // see examples at http://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_add_js/7
  // drupal_add_css(drupal_get_path('theme', 'kraken') . '/cmi_core.css');
  // drupal_add_js(drupal_get_path('theme', 'kraken') . '/cmi_core.js');
  // This is handy if you have some pages with large js or css weights
  // that you don't want to drag down the rest of your site with.

  // Since you probably created this pane to get at some data,
  // let's dpm() and take a look at it.  If you have the devel
  // module (http://drupal.org/project/devel) installed you will
  // see a very nice summary of your available data.

  // dpm($context->data);

  /**
  * Now let's assemble a set of variables to pass to our theme function.
  */

  // Placeholder for the purpose of this example...
  $body = t('o hai!');

  // But we might also define "footer"
  // $footer = t('your footer smells.');

  /**
  * So let's assume that all went well and we have a bunch of variables
  * ready to pass to our own custom theme function.
  */

  // Create an array of our variables.  We will pass this into our theme function.
  $variables = array(
    'body' => $body,
    // 'footer' => $footer, // we would just add our new variables here.
  );
  
  // Pass our variables to our theme function, and create our $content variable
  $content = theme('custompane_SAMPLEPANE', $variables);
  // Stick this in a $block as its content.
  $block->content = $content;
  // Return our data
  return $block;
  // Now let's go to our SAMLEPANE.tpl.php file and put our variables into markup...
}

/**
* Field rendering reference!  See http://bit.ly/U2dmGJ for more info
*/

// $image = render(field_view_field($entity_type, $entity_object, 'field_name',
//   $display = array(
//     'label' => 'hidden',
//     'settings' => array(
//       'image_style' => '390x330'
//     )
//   ), 
//   $langcode = NULL));

// label: (string) Position of the label. The default 'field' theme implementation supports the values 'inline', 'above' and 'hidden'. Defaults to 'above'.
// type: (string) The formatter to use. Defaults to the 'default_formatter' for the field type, specified in hook_field_info(). The default formatter will also be used if the requested formatter is not available.
// settings: (array) Settings specific to the formatter. Defaults to the formatter's default settings, specified in hook_field_formatter_info().
// weight: (float) The weight to assign to the renderable element. Defaults to 0.
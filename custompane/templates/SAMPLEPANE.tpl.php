<?php
/**
 * From the original SAMPLEPANE.inc file, we already have
 * the $body variable.  If we uncommented the $footer
 * variable we would have that available to us, too.
 */
?>


<div class="SAMPLEPANE">
	<div class='output'><?php print $body; ?></div>
</div>
#!/bin/bash
# Script to generate a panels pane
# from the SAMPLEPANE starter.
#

read -p "Enter the same of the custom pane: " panelname
echo "Creating pane $panelname..."

cat panes/SAMPLEPANE.inc | \
sed -e 's/SAMPLEPANE/'$panelname'/' > panes/$panelname.inc

cat templates/SAMPLEPANE.tpl.php | \
sed -e 's/SAMPLEPANE/'$panelname'/' > templates/$panelname.tpl.php

echo "Finished creating $panelname custom pane."
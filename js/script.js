Drupal.behaviors.kraken = {
    attach:function(){
    // alert('hi');
      (function ($, Drupal, window, document, undefined) {
        // alert('hi');


		function updateSlides(){
			var width = window.innerWidth;
			// Conditionally trigger different functions
			// when the page resize.  We can only have one
			// Window resize fucntion to we use this function
			// to spread it between other functions.  Make
			// them conditional to avoid executing code unnecessarily.
			if ($('body.front').length) {
				homepageSlides(width);
			}
			if ($('#flexbanner.slideshow').length) { //Conditional statement
				bannerSlides(width); // Call to appropriate function
			}    
		}

		// This is our global, throttled windo resize function
		// Use it to trigger events based on window size.
		var WIDTH = false;
		$(window).resize(function() {

		if(WIDTH !== false)
			clearTimeout(WIDTH);
			WIDTH = setTimeout(pageResize, 150); // time in miliseconds
		});


    })(jQuery, Drupal, this, this.document);
  }
}